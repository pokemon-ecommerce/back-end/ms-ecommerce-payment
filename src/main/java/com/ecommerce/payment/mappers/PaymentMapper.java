package com.ecommerce.payment.mappers;

import com.ecommerce.payment.model.dto.PaymentDto;
import com.ecommerce.payment.model.entity.Payment;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * <b>Class</b>: PaymentMapper <br/>
 * .
 *
 * @author Carlos <br/>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PaymentMapper {

  /**
   * paymentDtoToPayment .
   *
   * @param paymentDto .
   * @return
   */
  public static Payment paymentDtoToPayment(final PaymentDto paymentDto) {
    return Payment.builder()
        .orderId(paymentDto.getOrderId())
        .total(paymentDto.getTotal())
        .cardNumber(paymentDto.getCardNumber())
        .dueDate(paymentDto.getDueDate())
        .build();
  }

  /**
   * paymentToPaymentDto.
   *
   * @param payment .
   * @return
   */
  public static PaymentDto paymentToPaymentDto(final Payment payment) {
    return PaymentDto.builder()
        .orderId(payment.getOrderId())
        .total(payment.getTotal())
        .cardNumber(payment.getCardNumber())
        .dueDate(payment.getDueDate())
        .build();
  }

}
