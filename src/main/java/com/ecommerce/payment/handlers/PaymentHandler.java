package com.ecommerce.payment.handlers;

import com.ecommerce.payment.mappers.PaymentMapper;
import com.ecommerce.payment.model.dto.PaymentDto;
import com.ecommerce.payment.repository.PaymentRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: PaymentHandler <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Service
@RequiredArgsConstructor
public class PaymentHandler {
  private final PaymentRepository paymentRepository;

  /**
   * savePayment .
   * @param request .
   * @return
   */
  public Mono<ServerResponse> savePayment(final ServerRequest request) {
    return request.bodyToMono(PaymentDto.class)
        .map(PaymentMapper::paymentDtoToPayment)
        .flatMap(paymentRepository::insert)
        .map(PaymentMapper::paymentToPaymentDto)
        .flatMap(paymentDto -> ServerResponse.ok()
            .body(BodyInserters.fromValue(paymentDto)))
        .switchIfEmpty(ServerResponse.notFound().build());
  }

}
