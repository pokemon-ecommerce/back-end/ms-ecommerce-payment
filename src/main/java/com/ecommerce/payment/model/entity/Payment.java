package com.ecommerce.payment.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * <b>Class</b>: Payment <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Document(collection = "payments")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Payment {
  @Id
  private String id;
  private Long orderId;
  private Double total;
  private String cardNumber;
  private String dueDate;

}
