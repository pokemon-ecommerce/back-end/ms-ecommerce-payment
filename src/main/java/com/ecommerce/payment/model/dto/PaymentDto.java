package com.ecommerce.payment.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: PaymentDto <br/>
 * .
 *
 * @author Carlos <br/>
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PaymentDto {
  private Long orderId;
  private Double total;
  private String cardNumber;
  private String dueDate;
}
