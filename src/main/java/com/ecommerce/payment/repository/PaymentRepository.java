package com.ecommerce.payment.repository;

import com.ecommerce.payment.model.entity.Payment;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * <b>Class</b>: PaymentRepository <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Repository
public interface PaymentRepository extends ReactiveMongoRepository<Payment, String> {

}
