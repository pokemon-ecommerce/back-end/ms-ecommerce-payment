package com.ecommerce.payment.routes;

import com.ecommerce.payment.handlers.PaymentHandler;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicate;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * <b>Class</b>: PaymentRouter <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Configuration
public class PaymentRouter {
  private static final RequestPredicate ROUTE_ACCEPT = RequestPredicates.accept(MediaType.APPLICATION_JSON);

  /**
   * paymentRoutes .
   *
   * @param paymentHandler .
   * @return
   */
  @Bean
  public RouterFunction<ServerResponse> paymentRoutes(final PaymentHandler paymentHandler) {
    return RouterFunctions.route()
        .path("/payment", builder -> builder
            .POST("", ROUTE_ACCEPT, paymentHandler::savePayment))
        .build();
  }

}
